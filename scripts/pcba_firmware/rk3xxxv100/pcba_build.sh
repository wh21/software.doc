#!/bin/bash

######################################
#	Version: 2020-05-14
######################################
#	Version: 2020-08-03
#		1. fix compile error on rk3326
#		2. add some check
#		3. add rk3368 support
######################################

. build/envsetup.sh >/dev/null && setpaths

#set -x

TARGET_BOARD_PLATFORM=`get_build_var TARGET_BOARD_PLATFORM`
######################################
#	custom area start
######################################


##### RK3126C Platform ####
if [ $TARGET_BOARD_PLATFORM == "rk3126c" ]; then

##### TH863 8 #####
FIRMWARE_NAME=rk3126c_android_10.0_th863_8_bf3920_bf3920_rtl8723cs
KERNEL_CONFIG_PATH=arch/arm/configs/rk3126c_th863_8_defconfig
KERNEL_DTS_PATH=arch/arm/boot/dts/rk3126c-th863-8-avb.dts

CLK_DDR_FILE_PATH=arch/arm/boot/dts/rk312x.dtsi

## system-status-freq property in dmc node
#
#229                 system-status-freq = <
#230                         /*system status         freq(KHz)*/
#231                         SYS_STATUS_NORMAL       396000
#232                         SYS_STATUS_SUSPEND      396000
#233                         SYS_STATUS_REBOOT       396000
#234                 >;
#
#
SYS_FREQ_START_LINE=230
SYS_FREQ_END_LINE=$[$SYS_FREQ_START_LINE + 3]

## fixed freq opp table
#
#251                 opp-396000000 { 
#252                         opp-hz = /bits/ 64 <396000000>; 
#253                         opp-microvolt = <1250000>; 
#254                 };
#
OPP_FREQ_START_LINE=251
OPP_FREQ_END_LINE=$[$OPP_FREQ_START_LINE + 1]

##### RK3326 Platform ####
elif [ $TARGET_BOARD_PLATFORM == "rk3326" ]; then

##### TH98V #####
FIRMWARE_NAME=rk3326_android_10.0_th98v_gc0312_gc2385_rtl8723cs
KERNEL_CONFIG_PATH=arch/arm64/configs/rk3326_th98v_defconfig
KERNEL_DTS_PATH=arch/arm64/boot/dts/rockchip/rk3326-th98v.dts

##### AK47 #####
#FIRMWARE_NAME=rk3326_android_10.0_ak47_gc0312_gc2145_rtl8723cs
#KERNEL_CONFIG_PATH=arch/arm64/configs/rk3326_ak47_defconfig
#KERNEL_DTS_PATH=arch/arm64/boot/dts/rockchip/rk3326-ak47.dts

CLK_DDR_FILE_PATH=arch/arm64/boot/dts/rockchip/px30.dtsi

## system-status-freq property in dmc node
#
#1846                 system-status-freq = <
#1847                         /*system status         freq(KHz)*/
#1848                         SYS_STATUS_NORMAL       504000
#1849                         SYS_STATUS_REBOOT       504000
#1850                         SYS_STATUS_SUSPEND      504000
#1851                         SYS_STATUS_VIDEO_1080P  504000
#1852                         SYS_STATUS_BOOST        504000
#1853                         SYS_STATUS_ISP          504000
#1854                         SYS_STATUS_PERFORMANCE  504000
#1855                 >;
#
#
SYS_FREQ_START_LINE=1837
SYS_FREQ_END_LINE=$[$SYS_FREQ_START_LINE + 9]

## fixed freq opp table
#
#1911                 opp-504000000 {
#1912                         opp-hz = /bits/ 64 <504000000>;
#1913                         opp-microvolt = <975000>;
#1914                         opp-microvolt-L0 = <975000>;
#1915                         opp-microvolt-L1 = <975000>;
#1916                         opp-microvolt-L2 = <950000>;
#1917                         opp-microvolt-L3 = <950000>;
#1918                 };
#
OPP_FREQ_START_LINE=1900
OPP_FREQ_END_LINE=$[$OPP_FREQ_START_LINE + 1]

elif [ $TARGET_BOARD_PLATFORM == "rk3368" ]; then
##### TH863C V20 #####
FIRMWARE_NAME=rk3368_android_10.0_th863c_v20_gc032a_gc2145_rtl8723cs
KERNEL_CONFIG_PATH=arch/arm64/configs/rk3368a_817_defconfig
KERNEL_DTS_PATH=arch/arm64/boot/dts/rockchip/rk3368a-817-tablet.dts

#CLK_DDR_FILE_PATH=arch/arm64/boot/dts/rockchip/rk3368a-817-tablet.dts
CLK_DDR_FILE_PATH=arch/arm64/boot/dts/rockchip/rk3368.dtsi

## system-status-freq property in dmc node
#
#1254         system-status-freq = <
#1255             /*system status     freq(KHz)*/
#1256             SYS_STATUS_NORMAL   600000
#1257             SYS_STATUS_REBOOT   600000
#1258             SYS_STATUS_SUSPEND  600000
#1259             SYS_STATUS_VIDEO_1080P  600000
#1260             SYS_STATUS_VIDEO_4K 600000
#1261             SYS_STATUS_PERFORMANCE  600000
#1262             SYS_STATUS_BOOST    600000
#1263             SYS_STATUS_DUALVIEW 600000
#1264             SYS_STATUS_ISP      600000
#1265         >;
#
#
SYS_FREQ_START_LINE=1255
SYS_FREQ_END_LINE=$[$SYS_FREQ_START_LINE + 9]

## fixed freq opp table
#
#1306         opp-600000000 {
#1307             opp-hz = /bits/ 64 <600000000>;
#1308             opp-microvolt = <1100000>;
#1309         };
#
OPP_FREQ_START_LINE=1306
OPP_FREQ_END_LINE=$[$OPP_FREQ_START_LINE + 1]

fi

## fixed ddr freq
FREQ_START=500
FREQ_STEP=20
FREQ_NUM=10
######################################
#	custom area end
######################################

KERNEL_CONFIG=${KERNEL_CONFIG_PATH#*configs/}
if [ $TARGET_BOARD_PLATFORM == "rk3126c" ]; then
KERNEL_DTS=${KERNEL_DTS_PATH#*dts/}
elif [ $TARGET_BOARD_PLATFORM == "rk3326" ]; then
KERNEL_DTS=${KERNEL_DTS_PATH#*rockchip/}
KERNEL_CONFIG_MERGE="android-10-go.config rk3326.config"
elif [ $TARGET_BOARD_PLATFORM == "rk3368" ]; then
KERNEL_DTS=${KERNEL_DTS_PATH#*rockchip/}
KERNEL_CONFIG_MERGE="android-10-go.config"
fi
KERNEL_IMG=${KERNEL_DTS/dts/img}
MAKE_JOBS=32

SDK_PATH=`pwd`
KERNEL_ARCH=`get_build_var TARGET_ARCH`
TARGET_PRODUCT=`get_build_var TARGET_PRODUCT`
TARGET_ARCH_VARIANT=`get_build_var TARGET_ARCH_VARIANT`
BOARD_AVB_ENABLE=`get_build_var BOARD_AVB_ENABLE`
BOARD_KERNEL_CMDLINE=`get_build_var BOARD_KERNEL_CMDLINE`
ROCKCHIP_RECOVERYIMAGE_CMDLINE_ARGS=`get_build_var ROCKCHIP_RECOVERYIMAGE_CMDLINE_ARGS`
BOARD_BOOTIMG_HEADER_VERSION=`get_build_var BOARD_BOOTIMG_HEADER_VERSION`
PLATFORM_SECURITY_PATCH=`get_build_var PLATFORM_SECURITY_PATCH`
PLATFORM_VERSION=`get_build_var PLATFORM_VERSION`

IMAGE_PATH=rockdev/Image-$TARGET_PRODUCT
PACK_PATH=RKTools/linux/Linux_Pack_Firmware/rockdev-pcba
PACK_FIRST="true"
PACK_DATE=`date +%Y%m%d`

if [ ! -d $PACK_PATH ]; then
	echo -e "pcba firmware package dir is null, please add it!"
	exit 1;
fi

if [ ! -f "$OUT/dtbo.img" ]; then
	BOARD_DTBO_IMG=$OUT/rebuild-dtbo.img
else
	BOARD_DTBO_IMG=$OUT/dtbo.img
fi
cp -a $BOARD_DTBO_IMG $IMAGE_PATH/dtbo.img

if [ "$TARGET_ARCH_VARIANT" = "armv8-a" ]; then
	KERNEL_DEBUG=kernel/arch/arm64/boot/Image
	KERNEL_ARCH="arm64" # for rk3326
else
	KERNEL_DEBUG=kernel/arch/arm/boot/zImage
fi


make_kernel_config()
{
	if [ -z $KERNEL_ARCH ]; then
		echo -e "kernel arch is null, source and lunch first please!"
		exit 1;
	fi

	if [ -d kernel ]; then
		cd kernel
		make ARCH=$KERNEL_ARCH $KERNEL_CONFIG ${KERNEL_CONFIG_MERGE}

		if [ $? != 0 ]; then
			echo -e "make kernel defconfig failed!\n"
			exit 1;
		fi
	fi
}

make_kernel_img()
{
	make ARCH=$KERNEL_ARCH $KERNEL_IMG -j$MAKE_JOBS

	if [ $? != 0 ]; then
		echo -e "make kernel defconfig failed!\n"
		exit 1;
	fi

	cd $SDK_PATH
}

modify_ddr_freq()
{
	cd $SDK_PATH/kernel
	sed -i "${SYS_FREQ_START_LINE},${SYS_FREQ_END_LINE}s/$1/$2/" $CLK_DDR_FILE_PATH
	sed -i "${OPP_FREQ_START_LINE},${OPP_FREQ_END_LINE}s/$1/$2/" $CLK_DDR_FILE_PATH
}

make_recoveryimage()
{
	if [ "$BOARD_AVB_ENABLE" = "true" ]; then

		make recoveryimage -j32

		if [ $? != 0 ]; then
			echo -e "make recoveryimage failed!\n"
			exit 1;
		fi

		cp -arvf $OUT/recovery.img $IMAGE_PATH/recovery.img
	else
	echo "BOARD_AVB_ENABLE is false, make recovery.img from kernel && out."
	mkbootfs -d $OUT/system $OUT/recovery/root | minigzip > $OUT/ramdisk-recovery.img && \
	mkbootimg --kernel $KERNEL_DEBUG --ramdisk $OUT/ramdisk-recovery.img --second kernel/resource.img --os_version $PLATFORM_VERSION --header_version $BOARD_BOOTIMG_HEADER_VERSION --recovery_dtbo $BOARD_DTBO_IMG --os_patch_level $PLATFORM_SECURITY_PATCH --cmdline "$ROCKCHIP_RECOVERYIMAGE_CMDLINE_ARGS" --output $OUT/recovery.img && \
	cp -arvf $OUT/recovery.img $IMAGE_PATH/recovery.img
	fi
	
	echo -e "make recoveryimage ok!\n"

	cd $SDK_PATH
}

copy_wifi_module()
{
	find kernel/drivers/net/wireless/rockchip_wlan -iname "*.ko" | xargs -n1 -i cp -arvf {} $OUT/recovery/root/pcba/lib/modules/
	echo -e "copy wifi modules ok!\n"
}

pack_pcba_firmware()
{
	if [ ${PACK_FIRST} = "true" ]; then
		rm -rf $PACK_PATH/Image
		rm -rf $PACK_PATH/*.img

		if [ ! -d "$PACK_PATH/Image" ]; then
			mkdir -p $PACK_PATH/Image
		fi

		for IMG in  boot.img dtbo.img MiniLoaderAll.bin parameter.txt pcba_whole_misc.img recovery.img uboot.img trust.img vbmeta.img
		do
			cp -arvf $IMAGE_PATH/$IMG $PACK_PATH/Image/
		done
	else
		cp -arvf $IMAGE_PATH/recovery.img $SDK_PATH/$PACK_PATH/Image/
	fi

	PACK_FIRST="false"

	cd $PACK_PATH
	./mkupdate_${TARGET_BOARD_PLATFORM}.sh

	if [ $? != 0 ]; then
		echo -e "./mkupdate.sh failed!\n"
		exit 1;
	fi

	mv update.img ${FIRMWARE_NAME}_${1}M_$PACK_DATE.img
	echo -e "\nMaking ${FIRMWARE_NAME}_${1}M_$PACK_DATE.img OK."
	cd $SDK_PATH
}


FREQ_TMP=$FREQ_START

make_kernel_config

for ((i=0; i < $FREQ_NUM; i++))
do
	modify_ddr_freq	$FREQ_TMP $[$FREQ_START + $FREQ_STEP * i]
	make_kernel_img
	make_recoveryimage
	copy_wifi_module
	pack_pcba_firmware $[$FREQ_START + $FREQ_STEP * i]
	FREQ_TMP=$[$FREQ_START + $FREQ_STEP * i]
done

echo -e "\n Making all pcba test firmware OK!"

