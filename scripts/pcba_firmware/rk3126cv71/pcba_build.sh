#!/bin/bash

######################################
#	Version: 2020-06-02
######################################

. build/envsetup.sh >/dev/null && setpaths

#set -x

######################################
#	custom area start
######################################

##### TH1021DN #####
FIRMWARE_NAME=rk3126c_android_7.1_th1021dn_gc0329_gc2035_rtl8723cs
KERNEL_CONFIG_PATH=arch/arm/configs/rk3126_th1021dn_v4.0_RK816_8703bs_defconfig
KERNEL_DTS_PATH=arch/arm/boot/dts/rk3126-86v-rk816-thzy.dts

CLK_DDR_FILE_PATH=arch/arm/boot/dts/rk3126-86v-rk816-thzy.dts

##### TH863 7 #####
#FIRMWARE_NAME=rk3126c_android_7.1_th863_7_v30_sp0a19_hi256_rtl8723cs
#KERNEL_CONFIG_PATH=arch/arm/configs/rk3126_ak47bt_RK816_8703bs_defconfig
#KERNEL_DTS_PATH=arch/arm/boot/dts/rk3126-86v-rk816-thzy.dts
#
#CLK_DDR_FILE_PATH=arch/arm/boot/dts/rk3126-86v-rk816-thzy.dts

## system-status-freq property in dmc node
#
#170             freq-table = <
#171                 /*status        freq(KHz)*/
#172                 SYS_STATUS_NORMAL       380000
#173                 //SYS_STATUS_SUSPEND        200000
#174                 //SYS_STATUS_VIDEO_1080P  324000
#175                 //SYS_STATUS_VIDEO_4K     400000
#176                 //SYS_STATUS_PERFORMANCE  533000
#177                 //SYS_STATUS_DUALVIEW   400000
#178                 //SYS_STATUS_BOOST  324000
#179                 //SYS_STATUS_ISP        533000
#180                 >;
#
SYS_FREQ_START_LINE=170
SYS_FREQ_END_LINE=$[$SYS_FREQ_START_LINE + 0]

## fixed freq opp table
#
#162             operating-points = <
#163                 /* KHz    uV */
#164                 200000 1200000
#165                 360000 1200000
#166                 380000 1200000
#167                 456000 1250000
#168                 >;
#
OPP_FREQ_START_LINE=163
OPP_FREQ_END_LINE=$[$OPP_FREQ_START_LINE + 0]

## fixed ddr freq
FREQ_START=420
FREQ_STEP=20
FREQ_NUM=6
######################################
#	custom area end
######################################

KERNEL_CONFIG=${KERNEL_CONFIG_PATH#*configs/}
KERNEL_DTS=${KERNEL_DTS_PATH#*dts/}
KERNEL_IMG=${KERNEL_DTS/dts/img}
MAKE_JOBS=32

SDK_PATH=`pwd`
KERNEL_ARCH=`get_build_var TARGET_ARCH`
TARGET_PRODUCT=`get_build_var TARGET_PRODUCT`
IMAGE_PATH=rockdev/Image-$TARGET_PRODUCT
PACK_PATH=RKTools/linux/Linux_Pack_Firmware/rockdev-pcba
PACK_FIRST="true"
PACK_DATE=`date +%Y%m%d`

make_kernel_config()
{
	if [ -z $KERNEL_ARCH ]; then
		echo -e "kernel arch is null, source and lunch first please!"
		exit 1;
	fi

	if [ -d kernel ]; then
		cd kernel
		make ARCH=$KERNEL_ARCH $KERNEL_CONFIG
	fi
}

make_kernel_img()
{
	make ARCH=$KERNEL_ARCH $KERNEL_IMG -j$MAKE_JOBS
	cd $SDK_PATH
}

modify_ddr_freq()
{
	cd $SDK_PATH/kernel
	sed -i "${SYS_FREQ_START_LINE},${SYS_FREQ_END_LINE}s/$1/$2/" $CLK_DDR_FILE_PATH
	sed -i "${OPP_FREQ_START_LINE},${OPP_FREQ_END_LINE}s/$1/$2/" $CLK_DDR_FILE_PATH
}

pack_pcba_firmware()
{
	if [ ! -d $PACK_PATH ]; then
		echo -e "pcba firmware package dir is null, please add it!"
		exit 1;
	fi

	if [ ${PACK_FIRST} = "true" ]; then
		rm -rf $PACK_PATH/Image
		rm -rf $PACK_PATH/*.img

		if [ ! -d "$PACK_PATH/Image" ]; then
			mkdir -p $PACK_PATH/Image
		fi

		for IMG in  kernel.img resource.img boot.img MiniLoaderAll.bin parameter.txt pcba_whole_misc.img recovery.img uboot.img trust.img
		do
			cp -arvf $IMAGE_PATH/$IMG $PACK_PATH/Image/
		done
	else
		cp -arvf $SDK_PATH/kernel/resource.img $SDK_PATH/$PACK_PATH/Image/
	fi
	PACK_FIRST="false"

	cd $PACK_PATH
	./mkupdate.sh

	mv update.img ${FIRMWARE_NAME}_${1}M_$PACK_DATE.img
	echo -e "\nMaking ${FIRMWARE_NAME}_${1}M_$PACK_DATE.img OK."
	cd $SDK_PATH
}


FREQ_TMP=$FREQ_START
# It's no need to make kernel config, because selinux is no compiled manually
# make_kernel_config

for ((i=0; i < $FREQ_NUM; i++))
do
	modify_ddr_freq	$FREQ_TMP $[$FREQ_START + $FREQ_STEP * i]
	make_kernel_img
	pack_pcba_firmware $[$FREQ_START + $FREQ_STEP * i]
	FREQ_TMP=$[$FREQ_START + $FREQ_STEP * i]
done

echo -e "\n Making all pcba test firmware OK!"

