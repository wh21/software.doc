﻿一、背景说明
给生产部门提供的小固件，需要有多个频率。如果手工修改文件再编译、拷贝、打包、重命名的话，需要花费比较多时间精力。此脚本将这一系列的步骤，自动化处理，降低这项任务的繁琐程度，尽量利用服务器的强大性能，解放劳动力，达到偷懒的目的。

二、需要注意的地方

1. 小固件的功能已经确认开发完成，满足PCBA测试需求且无问题，rockdev/目录下的散包是最新的；
2. Linux下的固件打包工具可用于打包pcba固件，如RKTools/linux/Linux_Pack_Firmware/rockdev-pcba，它与大固件打包工具的区别在于package-file，需要做出如下修改：
misc		Image/pcba_whole_misc.img
#system		Image/system.img
#vendor		Image/vendor.img
#oem		Image/oem.img

另外，需要注意： afptool、rkImageMaker、mkupdate.sh都需要赋予可执行权限！

3. 需要手动修改好第一个DDR频率，例如将场景变频全部设为450M，auto-freq-en改为0,再把dmc-opp-table中除450M的其它频点disabled掉；

4. 修改脚本文件中的定制化区域，其它区域可以根据具体情况修改，一般不需要修改：
4.1 固件名、kernel defconfig文件路径、dts文件路径、dmc所在文件路径;
4.2 指定场景变频属性和频率opp-table所在的起始行数;
4.3 指定第3步中的起始频率，如450M，频率步进大小和需要制作的小固件个数；

5. 在第1步骤的sdk编译环境中，源码根目录下执行此脚本。否则需要先执行source javaenv.sh； source build/envsetup.sh； lunch等操作；
