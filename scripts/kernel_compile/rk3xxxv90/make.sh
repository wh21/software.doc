#!/bin/bash
#########################################################
###	This is for Android 9.0 on RK3126C/RK3326
#########################################################
###	Usage: ./make.sh, enter number to choose.
###		Or ./make.sh last if you need use last
###		compile config
#########################################################

#########################################################
###	Version: 2020-03-24
###		1. add rk3326 m1011qr_v30
#########################################################
#########################################################
###	Version: 2020-03-08
###		1. support rk3126c
###		2. support rk3326
#########################################################

#set -x

RK3126C_BOARD_ARRAY=(ak47 th1021dn th863_10)
RK3126C_BOARD_COUNT=${#RK3126C_BOARD_ARRAY[*]}
RK3326_BOARD_ARRAY=(ak47 th1021dn th1021dn_v20 th863b_v31_7 th863b_7 th863b_8 th863b_10 th7926_7 th7926_9 th700 mt1011 m1011qr m1011qr_v30)
RK3326_BOARD_COUNT=${#RK3326_BOARD_ARRAY[*]}

PLATFORM_ARCH=arm
KERNEL_DEFCONFIG=rk3126c_ak47_rk816_8703bs_defconfig
KERNEL_IMG=rk3126c-thzy-ak47bt.img
MAKE_CONFIG_FILE=.make.config


print_platform_menu()
{
	echo -e "\n Making boot.img\n"
	echo -e " Choose platform:"
	echo -e "\t 1. rk3126c"
	echo -e "\t 2. rk3326"
	echo -e "\t 3. Exit menu"
	echo -en "\t Enter an option: "
	read platform_option
}

print_rk3126c_board_menu()
{
	echo -e " Choose board:"
	for ((i = 0; i < $RK3126C_BOARD_COUNT; i++))
	do
		echo -e "\t $i. ${RK3126C_BOARD_ARRAY[$i]}"
	done
	echo -en "\t Enter an option: "
	read board_option
}

print_rk3326_board_menu()
{
	echo -e " Choose board:"
	for ((i = 0; i < $RK3326_BOARD_COUNT; i++))
	do
		echo -e "\t $i. ${RK3326_BOARD_ARRAY[$i]}"
	done
	echo -en "\t Enter an option: "
	read board_option
}

choose_rk3126c_board()
{
	case $board_option in

		0)
			KERNEL_DEFCONFIG=rk3126c_${RK3126C_BOARD_ARRAY[$board_option]}_rk816_8703bs_defconfig
			KERNEL_IMG=rk3126c-${RK3126C_BOARD_ARRAY[$board_option]}-avb.img
			;;
		1)
			KERNEL_DEFCONFIG=rk3126c_${RK3126C_BOARD_ARRAY[$board_option]}_rk816_8703bs_defconfig
			KERNEL_IMG=rk3126c-${RK3126C_BOARD_ARRAY[$board_option]}-avb.img
			;;
		2)
			KERNEL_DEFCONFIG=rk3126c_${RK3126C_BOARD_ARRAY[$board_option]}_defconfig
			KERNEL_IMG=rk3126c-${RK3126C_BOARD_ARRAY[$board_option]}-avb.img
			KERNEL_IMG=${KERNEL_IMG//_/-}	# rk3126c-th863b_10-avb.img  --> rk3126c-th863b-10-avb.img
			;;
		# Attention: add case item above, if you add a new board!!!
		*)
			echo -e "\n unknown option"
			exit 1;
			;;
	esac

	echo -e "\n board is ${RK3126C_BOARD_ARRAY[$board_option]}"
}

choose_rk3326_board()
{
	case $board_option in

		0)
			KERNEL_IMG=rk3326-${RK3326_BOARD_ARRAY[$board_option]}-avb.img
			;;
		1)
			KERNEL_IMG=rk3326-${RK3326_BOARD_ARRAY[$board_option]}-avb.img
			;;
		2)
			KERNEL_IMG=rk3326-${RK3326_BOARD_ARRAY[$board_option]}-avb.img
			;;
		3)
			KERNEL_IMG=rk3326-${RK3326_BOARD_ARRAY[$board_option]}-avb.img
			KERNEL_IMG=${KERNEL_IMG//_/-}	# rk3326-th863b_v31_7-avb.img  --> rk3326-th863b--v31-7-avb.img
			;;
		4)
			KERNEL_IMG=rk3326-${RK3326_BOARD_ARRAY[$board_option]}-avb.img
			KERNEL_IMG=${KERNEL_IMG//_/-}	# rk3326-th863b_7-avb.img  --> rk3326-th863b-7-avb.img
			;;
		5)
			KERNEL_IMG=rk3326-${RK3326_BOARD_ARRAY[$board_option]}-avb.img
			KERNEL_IMG=${KERNEL_IMG//_/-}	# rk3326-th863b_8-avb.img  --> rk3326-th863b-8-avb.img
			;;
		6)
			KERNEL_IMG=rk3326-${RK3326_BOARD_ARRAY[$board_option]}-avb.img
			KERNEL_IMG=${KERNEL_IMG//_/-}	# rk3326-th863b_10-avb.img  --> rk3326-th863b-10-avb.img
			;;
		7)
			KERNEL_IMG=rk3326-${RK3326_BOARD_ARRAY[$board_option]}-avb.img
			;;
		8)
			KERNEL_IMG=rk3326-${RK3326_BOARD_ARRAY[$board_option]}-avb.img
			;;
		9)
			KERNEL_IMG=rk3326-${RK3326_BOARD_ARRAY[$board_option]}-avb.img
			;;
		10)
			KERNEL_IMG=rk3326-${RK3326_BOARD_ARRAY[$board_option]}-avb.img
			;;
		11)
			KERNEL_IMG=rk3326-${RK3326_BOARD_ARRAY[$board_option]}-avb.img
			;;
		12)
			KERNEL_IMG=rk3326-${RK3326_BOARD_ARRAY[$board_option]}-avb.img
			KERNEL_IMG=${KERNEL_IMG//_/-}	# rk3326-m1011qr_v30-avb.img  --> rk3326-m1011qr-v30-10-avb.img
			;;
		# Attention: add case item above, if you add a new board!!!
		*)
			echo -e "\n unknown option"
			exit 1;
			;;

	esac

	echo -e "\n board is ${RK3326_BOARD_ARRAY[$board_option]}"
	KERNEL_DEFCONFIG=rk3326_${RK3326_BOARD_ARRAY[$board_option]}_defconfig
}

choose_platform()
{
	case $platform_option in

		1)
			echo -e "\n platform is rk3126c"

			PLATFORM_ARCH=arm

			print_rk3126c_board_menu
			choose_rk3126c_board
			;;
		2)
			echo -e "\n platform is rk3326"

			PLATFORM_ARCH=arm64

			print_rk3326_board_menu
			choose_rk3326_board
			;;
		3)
			echo -e "\n Exit menu"
			exit 0;
			;;
		*)
			echo -e "\n unknown option"
			exit 1;
			;;

	esac
}

make_kernel_defconfig()
{
	echo -en "\n Need make to update kernel .config? [y/n]"
	read need_option

	case $need_option in
		y)
			echo -e "\n update kernel .config\n"
			echo -e " make ARCH=${PLATFORM_ARCH} ${KERNEL_DEFCONFIG} \n"
			make ARCH=${PLATFORM_ARCH} ${KERNEL_DEFCONFIG}
			;;
		n)
			echo -e "\n not update kernel .config\n"
			;;
		*)
			echo -e "\n unknown option"
			exit 1;
			;;
	esac
}

make_kernel_img()
{
	echo -e "\n Start to make kernel img\n"
	echo -e " make ARCH=${PLATFORM_ARCH} ${KERNEL_IMG} -j32\n"
	make ARCH=${PLATFORM_ARCH} ${KERNEL_IMG} -j32
}

write_config()
{
	echo -e "$PLATFORM_ARCH" > $MAKE_CONFIG_FILE
	echo -e "$KERNEL_DEFCONFIG" >> $MAKE_CONFIG_FILE
	echo -e "$KERNEL_IMG" >> $MAKE_CONFIG_FILE
}
read_config()
{
	read -p "platform arch:" PLATFORM_ARCH
	read -p "kernel config:" KERNEL_DEFCONFIG
	read -p "platform KERNEL_IMG:" KERNEL_IMG
}
print_usage()
{
	echo -e "\n  Usage: exec ./make.sh, config manually"
	echo -e "\tor exec ./make.sh last for using last config to complie"
}

if [ $# -gt 1 ]; then
	print_usage
	exit 1;
elif [ $# -eq 1 ]; then
	if [ $1 = "last" ]; then
		read_config < $MAKE_CONFIG_FILE
	else
		print_usage
		exit 1;
	fi
else
	print_platform_menu
	choose_platform
	write_config
fi
make_kernel_defconfig
make_kernel_img

if [ $? == 0 ]; then
	echo -e "\n Making boot.img ok!!!"
else
	echo -e "\n Making boot.img failed!!!"
fi
