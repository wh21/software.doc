#!/bin/bash
#########################################################
###	Usage: ./make.sh, enter number to choose.
###		Or ./make.sh last if you need use last
###		compile config
#########################################################

#########################################################
###	Version: 2020-01-02
###		1. add last config support
###		2. add sample boot info
#########################################################
###	Version: 2019-12-29
#########################################################

#set -x

RK3126C_BOARD_ARRAY=(ak47 th1021dn th863_7 th863_8 th863_10 th98v)
RK3126C_BOARD_COUNT=${#RK3126C_BOARD_ARRAY[*]}
RK3326_BOARD_ARRAY=(ak47 th1021dn th863b_7 th863b_v31_7 th863b_8 th863b_10 th700 th98v)
RK3326_BOARD_COUNT=${#RK3326_BOARD_ARRAY[*]}
RK3368_BOARD_ARRAY=(th863c_10 rk3368a_817)
RK3368_BOARD_COUNT=${#RK3368_BOARD_ARRAY[*]}

PLATFORM_ARCH=arm
ANDROID_TYPE=go
KERNEL_DEFCONFIG=rk3126c_ak47_defconfig
KERNEL_IMG=rk3126c-ak47-avb.img
ANDROID_CONFIG=android-10-go.config
SAMPLE_BOOT=rk3126c_boot.img	# copy from rockdev/Image-rk3126c_qgo/boot.img
#SAMPLE_BOOT=rk3326_boot.img	# copy from rockdev/Image-rk3326_qgo/boot.img
MAKE_CONFIG_FILE=.make.config


print_platform_menu()
{
	echo -e "\n Making boot.img\n"
	echo -e " Choose platform:"
	echo -e "\t 1. rk3126c"
	echo -e "\t 2. rk3326"
	echo -e "\t 3. rk3368"
	echo -e "\t 4. Exit menu"
	echo -en "\t Enter an option: "
	read platform_option
}

print_rk3126c_board_menu()
{
	echo -e " Choose board:"
	for ((i = 0; i < $RK3126C_BOARD_COUNT; i++))
	do
		echo -e "\t $i. ${RK3126C_BOARD_ARRAY[$i]}"
	done
	echo -en "\t Enter an option: "
	read board_option
}

print_rk3326_board_menu()
{
	echo -e " Choose board:"
	for ((i = 0; i < $RK3326_BOARD_COUNT; i++))
	do
		echo -e "\t $i. ${RK3326_BOARD_ARRAY[$i]}"
	done
	echo -en "\t Enter an option: "
	read board_option
}

print_rk3368_board_menu()
{
	echo -e " Choose board:"
	for ((i = 0; i < $RK3368_BOARD_COUNT; i++))
	do
		echo -e "\t $i. ${RK3368_BOARD_ARRAY[$i]}"
	done
	echo -en "\t Enter an option: "
	read board_option
}

choose_rk3126c_board()
{
	case $board_option in

		0) 
			# you can add some special code here if you need
			;;
		1) 
			;;
		2) 
			;;
		3) 
			;;
		4) 
			;;
		5) 
			;;
		# Attention: add case item above, if you add a new board!!!
		*)
			echo -e "\n unknown option"
			exit 1;
			;;
	esac

	echo -e "\n board is ${RK3126C_BOARD_ARRAY[$board_option]}"
	KERNEL_DEFCONFIG=rk3126c_${RK3126C_BOARD_ARRAY[$board_option]}_defconfig
	KERNEL_IMG=rk3126c-${RK3126C_BOARD_ARRAY[$board_option]}-avb.img
	KERNEL_IMG=${KERNEL_IMG//_/-}
}

choose_rk3326_board()
{
	case $board_option in

		0) 
			# you can add some special code here if you need
			;;
		1) 
			;;
		2) 
			;;
		3) 
			;;
		4) 
			;;
		5) 
			;;
		6) 
			;;
		7) 
			;;
		# Attention: add case item above, if you add a new board!!!
		*)
			echo -e "\n unknown option"
			exit 1;
			;;

	esac

	echo -e "\n board is ${RK3326_BOARD_ARRAY[$board_option]}"
	KERNEL_DEFCONFIG=rk3326_${RK3326_BOARD_ARRAY[$board_option]}_defconfig
	KERNEL_IMG=rk3326-${RK3326_BOARD_ARRAY[$board_option]}.img
	KERNEL_IMG=${KERNEL_IMG//_/-}
}

choose_rk3368_board()
{
	case $board_option in

		0)
			# you can add some special code here if you need
			KERNEL_DEFCONFIG=rk3368_${RK3368_BOARD_ARRAY[$board_option]}_defconfig
			KERNEL_IMG=rk3368-${RK3368_BOARD_ARRAY[$board_option]}-avb.img
			KERNEL_IMG=${KERNEL_IMG//_/-}
			;;
		1)
			KERNEL_DEFCONFIG=${RK3368_BOARD_ARRAY[$board_option]}_defconfig
			KERNEL_IMG=${RK3368_BOARD_ARRAY[$board_option]}-tablet.img
			KERNEL_IMG=${KERNEL_IMG//_/-}
			;;
		# Attention: add case item above, if you add a new board!!!
		*)
			echo -e "\n unknown option"
			exit 1;
			;;

	esac

	echo -e "\n board is ${RK3368_BOARD_ARRAY[$board_option]}"
}

choose_platform()
{
	case $platform_option in

		1) 
			echo -e "\n platform is rk3126c"

			PLATFORM_ARCH=arm
			PLATFORM_CONFIG=
			RK3126C_SAMPLE_BOOT=rk3126c_boot.img

			if [ -f ${RK3126C_SAMPLE_BOOT} ]; then
				SAMPLE_BOOT=${RK3126C_SAMPLE_BOOT}
				echo -e "\n Use $SAMPLE_BOOT as sample to make\n"
			else
				SAMPLE_BOOT=
				echo -e "\n no sample boot rk3126c_boot.img in kernel src dir, copy from rockdev/Image-rk3126c_**go/boot.img please!!!\n"
			fi

			print_rk3126c_board_menu
			choose_rk3126c_board
			;;
		2) 
			echo -e "\n platform is rk3326"

			PLATFORM_ARCH=arm64
			PLATFORM_CONFIG=rk3326.config
			RK3326_SAMPLE_BOOT=rk3326_boot.img

			if [ -f ${RK3326_SAMPLE_BOOT} ]; then
				SAMPLE_BOOT=${RK3326_SAMPLE_BOOT}
				echo -e "\n Use $SAMPLE_BOOT as sample to make\n"
			else
				SAMPLE_BOOT=
				echo -e "\n no sample boot rk3326_boot.img in kernel src dir, copy from rockdev/Image-rk3326_**go/boot.img please!!!\n"
			fi

			print_rk3326_board_menu
			choose_rk3326_board
			;;
		3)
			echo -e "\n platform is rk3368"

			PLATFORM_ARCH=arm64
			PLATFORM_CONFIG=
			RK3368_SAMPLE_BOOT=rk3368_boot.img

			if [ -f ${RK3368_SAMPLE_BOOT} ]; then
				SAMPLE_BOOT=${RK3368_SAMPLE_BOOT}
				echo -e "\n Use $SAMPLE_BOOT as sample to make\n"
			else
				SAMPLE_BOOT=
				echo -e "\n no sample boot rk3368_boot.img in kernel src dir, copy from rockdev/Image-rk3368_**go/boot.img please!!!\n"
			fi

			print_rk3368_board_menu
			choose_rk3368_board
			;;
		4)
			echo -e "\n Exit menu"
			exit 0;
			;;
		*)
			echo -e "\n unknown option"
			exit 1;
			;;

	esac
}

choose_android_type()
{
	echo -e "\n Choose android type:"
	echo -e "\t 0. go"
	echo -e "\t 1. normal"
	echo -en "\t Enter an option: "
	#read -n 1 type_option
	read type_option

	case $type_option in
		0)
			echo -e "\n choose android go version"
			ANDROID_TYPE=go
			;;
		1)
			echo -e "\n choose android normal version"
			ANDROID_TYPE=normal
			;;
		*)
			echo -e "\n unknown option"
			exit 1;
			;;

	esac

	if [[ ${ANDROID_TYPE} == "go" ]]; then
		ANDROID_CONFIG=android-10-go.config
	else
		ANDROID_CONFIG=android-10.config
	fi
}

make_kernel_defconfig()
{
	echo -en "\n Need make to update kernel .config? [y/n]"
	read need_option

	case $need_option in
		y)
			echo -e "\n update kernel .config\n"
			echo -e " make ARCH=${PLATFORM_ARCH} ${KERNEL_DEFCONFIG} ${ANDROID_CONFIG} ${PLATFORM_CONFIG} \n"
			make ARCH=${PLATFORM_ARCH} ${KERNEL_DEFCONFIG} ${ANDROID_CONFIG} ${PLATFORM_CONFIG}
			;;
		n)
			echo -e "\n not update kernel .config\n"
			;;
		*)
			echo -e "\n unknown option"
			exit 1;
			;;
	esac
}

make_kernel_img()
{
	echo -e "\n Start to make kernel img\n"
	echo -e " make ARCH=${PLATFORM_ARCH} BOOT_IMG=${SAMPLE_BOOT} ${KERNEL_IMG} -j32\n"
	make ARCH=${PLATFORM_ARCH} BOOT_IMG=${SAMPLE_BOOT} ${KERNEL_IMG} -j32
}

write_config()
{
	echo -e "$PLATFORM_ARCH" > $MAKE_CONFIG_FILE
	echo -e "$KERNEL_DEFCONFIG" >> $MAKE_CONFIG_FILE
	echo -e "$ANDROID_CONFIG" >> $MAKE_CONFIG_FILE
	echo -e "$PLATFORM_CONFIG" >> $MAKE_CONFIG_FILE
	echo -e "$SAMPLE_BOOT" >> $MAKE_CONFIG_FILE
	echo -e "$KERNEL_IMG" >> $MAKE_CONFIG_FILE
}
read_config()
{
	read -p "platform arch:" PLATFORM_ARCH
	read -p "kernel config:" KERNEL_DEFCONFIG
	read -p "android config:" ANDROID_CONFIG
	read -p "platform config:" PLATFORM_CONFIG
	read -p "platform SAMPLE_BOOT:" SAMPLE_BOOT
	read -p "platform KERNEL_IMG:" KERNEL_IMG
}
print_usage()
{
	echo -e "\n  Usage: exec ./make.sh, config manually"
	echo -e "\tor exec ./make.sh last for using last config to complie"
}
if [ $# -gt 1 ]; then
	print_usage
	exit 1;
elif [ $# -eq 1 ]; then
	if [ $1 = "last" ]; then
		read_config < $MAKE_CONFIG_FILE
	else
		print_usage
		exit 1;
	fi
else
	print_platform_menu
	choose_platform
	choose_android_type
	write_config
fi
make_kernel_defconfig
make_kernel_img

if [ $? == 0 ]; then
	echo -e "\n Making boot.img ok!!!"
else
	echo -e "\n Making boot.img failed!!!"
fi
