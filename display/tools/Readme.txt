此文档记录说明调试display模块时，可以提高效率的一些工具。

调试MIPI屏时，屏厂提供的初始化代码格式各异，需要处理成RK对应平台的格式，这里提供一些方法
和思路。
1. 拷贝一份初始化代码文件，寻找其中的内容格式规律，找出有用的部分；
2. 通过vim编辑器，将无用的部分替换或者删除掉，得到一个特定内容格式文件（寄存器、参数）；
无用的部分包括不限于： 字符串、注释内容、空格、空行；
例如： 
（1）初始化代码原文件，需要的寄存器参数内容文件，kernel4.4平台格式如下

regw(0x80,0x88);
regw(0x81,0x0B);
regw(0x82,0x69);
regw(0x83,0x88);
regw(0x84,0x54);
regw(0x85,0x45);
regw(0x86,0x88);

	||
	||
	
80 88
81 0B
82 69
83 88
84 54
85 45
86 88

	||
	||
	
15 00 02 80 88
15 00 02 81 0B
15 00 02 82 69
15 00 02 83 88
15 00 02 84 54
15 00 02 85 45
15 00 02 86 88

（2）初始化代码原文件，需要的寄存器参数内容文件，kernel3.10平台格式如下
SSD_Single(0xE1,0x93);
SSD_Single(0xE2,0x65);
SSD_Single(0xE3,0xF8);
SSD_Single(0x80,0x03);

	||
	||

0xE1 0x93
0xE2 0x65
0xE3 0xF8
0x80 0x03

	||
	||
	
rockchip,on-cmds2 {
	compatible = "rockchip,on-cmds";
	rockchip,cmd_type = <LPDT>;
	rockchip,dsi_id = <0>;
	rockchip,cmd = <0x15 0xE1 0x93>;
	rockchip,cmd_delay = <0>;
};

rockchip,on-cmds3 {
	compatible = "rockchip,on-cmds";
	rockchip,cmd_type = <LPDT>;
	rockchip,dsi_id = <0>;
	rockchip,cmd = <0x15 0xE2 0x65>;
	rockchip,cmd_delay = <0>;
};

rockchip,on-cmds4 {
	compatible = "rockchip,on-cmds";
	rockchip,cmd_type = <LPDT>;
	rockchip,dsi_id = <0>;
	rockchip,cmd = <0x15 0xE3 0xF8>;
	rockchip,cmd_delay = <0>;
};

rockchip,on-cmds5 {
	compatible = "rockchip,on-cmds";
	rockchip,cmd_type = <LPDT>;
	rockchip,dsi_id = <0>;
	rockchip,cmd = <0x15 0x80 0x03>;
	rockchip,cmd_delay = <0>;
};


注明： kernel3.10对应的中间内容格式文件，需要加0x前缀表示十六进制，kernel4.4对应的中间内容格式文件不需要！

3. 把上一步得到的文件，进行文件格式转换，用dos2unix命令，dos转换成unix文档；此步非常重要！！！
4. 通过awk脚本进行批量行处理，转换成RK对应kernel平台需要的格式。
例如通过命令：awk -f mipi_kernel3.10.awk  20810800210180_init_code.txt
执行成功后，会在终端打印批量处理后的内容，可以重定向到文件，不影响源文件。


使用vim编辑器处理初始化代码文件时，可能会用到的一些技巧如下，仅供参考：
1. 删空白行： g/^$/d
2. 删行首空格: s/^\s\+//g
3. 删行末空格： s/\s\+$//g
其中\s表示空格，\+表示多个，^表示行首，$表示行末。

4. 全局匹配行首是//开头的行，并将其删除：g/^\/\//d
5. 行内字符串匹配删除：%s/\/\/.*//g，行内删除//之后的所有字符串

如有不理解之处，可以多交流。



